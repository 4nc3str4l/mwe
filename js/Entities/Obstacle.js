/**
 * Created by cristian on 02/02/2015.
 */
/**
 * The player class
 * @param xOrigin
 * @param yOrigin
 * @constructor
 */
var Obstacle = function (xOrigin,yOrigin) {

    this.rect = new Rectangle(xOrigin, yOrigin, 200, 200);

    this.update = function () {
        this.rect.pos.x++;
        console.log(this.rect.pos.x);
    };

    this.render = function () {
        context.fillStyle = "white";
        context.fillRect(this.rect.pos.x, this.rect.pos.y, this.rect.w, this.rect.h);
    };
};