var Player = function(){

  this.mySprite = loadSprite("images/spaceship.png");
  this.transform = new Transform(0, 0, this.mySprite.width, this.mySprite.height, 0);


  this.update = function()
  {

    var deltaY = mousePos.y - canvas.height / 2 ;
    var deltaX = mousePos.x - canvas.width / 2 ;

    this.transform.rotation = Math.atan2(deltaY, deltaX) * 180 / Math.PI;
    if(this.transform.rotation < 0 ){
      this.transform.rotation = -this.transform.rotation;
    }else{
      this.transform.rotation = 360 - this.transform.rotation;
    }

    var moveVec = new Vector2();
    if(keys.indexOf(key.up) > -1){
      this.transform.rect.pos.x += Math.cos(this.transform.rotation).toFixed(2) * 2;
      this.transform.rect.pos.y += Math.sin(this.transform.rotation).toFixed(2) * 2;
    }
    if(keys.indexOf(key.down) > -1){
      moveVec.x = -Math.cos(this.transform.rotation) * 2;
      moveVec.y = -Math.sin(this.transform.rotation) * 2;
    }
    if(keys.indexOf(key.left) > -1){
      moveVec.x = Math.cos(this.transform.rotation - 90) * 2;
      moveVec.y = Math.sin(this.transform.rotation - 90) * 2;
    }
    if(keys.indexOf(key.right) > -1){
      moveVec.x = Math.cos(this.transform.rotation + 90) * 2;
      moveVec.y = Math.sin(this.transform.rotation +90) * 2;

    }
    console.log("Player rotation = "+  this.transform.rotation);
    console.log("Player position = (" + this.transform.rect.pos.x + "," + this.transform.rect.pos.y+")");
    this.transform.rotation = Math.atan2(deltaY, deltaX) * 180 / Math.PI;
  }

  this.render = function()
  {
      // draw sprites
      drawSprite(this.mySprite, canvas.width/2, canvas.height/2, this.transform.rotation, 2);
  }
}
