var Projectile = function(){

    //create a new image for the map background
    var img = new Image();
    var img = "images/projectile.png";

    //the original position of our map
    var screenPosx = 0;
    var screenPosy = 0;

    this.render = function(x, y){

        context.drawImage(
            img,
            0, 0,
            zoomX, zoomY,
            screenPosx, screenPosy,
            canvas.width, canvas.height
        );
    }
}
