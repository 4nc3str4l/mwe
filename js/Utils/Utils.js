//############################################################################
//#                      Suport Functions And Classes                        #
//############################################################################

Object.defineProperty(Array.prototype, "remove", {
    enumerable: false,
    value: function (item) {
        var removeCounter = 0;

        for (var index = 0; index < this.length; index++) {
            if (this[index] === item) {
                this.splice(index, 1);
                removeCounter++;
                index--;
            }
        }
        return removeCounter;
    }
});

// GLOBAL VARIABLES USED ON THE ENTIRE GAME
var canvas = document.getElementById("canvas");
var context = canvas.getContext("2d");

var zoomFactor = new Vector2();
var mousePos = new Vector2();
var defaultCanvasSize = new Vector2(980, 551);

var keys = [];

var screnHeight = 0;
var screenWidth = 0;
var fps = 0;
var ups = 0;

var paused = false;
var muted = true;
var DEBUG = true;



/**
 * Key definitions (no more keycodes)
 * @constructor
 */
function Key(){
    this.up = 87;
    this.down = 83;
    this.right = 68;
    this.left = 65;
}

function mute(){
    muted = !muted;
}

/**
 * Sound handler
 * @constructor
 */
function Sound(){
    this.end = new Audio("sounds/finish.wav");
}

/**
 * This function is gonna handle time
 * @constructor
 */
function Time(){
    this.d = new Date();
    this.gameStart = this.d.getTime();
    this.deltaTime = this.d.getTime();
}


//############################################################################
//#                       KEY FUNCTIONS AND EVENTS                           #
//############################################################################
function keyDown(event){
    keys.push(event.which);

    if(event.which == 118){
      DEBUG = !DEBUG;
      if(DEBUG){
        console.log("DEBUG MODE: ON");
        $("#debugBox").css("visibility", "visible");
      }else{
        console.log("DEBUG MODE: OFF");
        $("#debugBox").css("visibility", "hidden");
      }
    }
}

function keyUp(event) {
    keys.remove(event.which);
    if (!muted) {
        new Sound().end.play();
    }
}

//gets mouse position over the canvas
function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}
//resize canvas size
resizeScreen();

//Add the listeners for the keys
document.addEventListener("keydown", keyDown);
document.addEventListener("keyup", keyUp);

//Add the mouse listeners on the canvas

canvas.addEventListener('mousemove', function(evt) {
    var pos = getMousePos(canvas, evt);
    mousePos.x = pos.x;
    mousePos.y = pos.y;
}, false);

//resizes screen using aspect ratio
function resizeScreen(){

    //get the screen size
    screnHeight = $(window).height() - 20;
    screenWidth = $(window).width() - 20;

    //Aspect ratio calculus

    var valueHeight = calculate16_9Height(screenWidth);
    var valueWidth = screenWidth;

    if(valueHeight > screnHeight){
      valueWidth = calculate16_9Width(screnHeight);
      valueHeight = calculate16_9Height(valueWidth);
    }

    //Assign the new mesures to the canvas
    canvas.width = valueWidth;
    canvas.height = valueHeight;

    //calculate the zoomfactor
    zoomFactor = new Vector2();
    zoomFactor.x = canvas.width / defaultCanvasSize.x;
    zoomFactor.y = canvas.height / defaultCanvasSize.y;

}

function calculate16_9Width(height){
  return Math.round((height/9)*16);
}

function calculate16_9Height(width){
  return Math.round((width/16)*9);
}

//detects when you are resizing the screen and tries to adjust the game to it
$( window ).resize(function() {
    resizeScreen();
});

// [imageName] image file name
function loadSprite(imageName)
{
    // create new image object
    var image = new Image();
    // load image
    image.src = imageName;
    // return image object
    return image;
}

// [imageObject] this is image object which is returned loadImage
// [x] screen x coordinate
// [y] screen y coordinate
// [rotation] rotation angle in radians (normal value 0.0)
// [scale] sprite scale (normal value 1.0)
function drawSprite(imageObject, x, y, rotation, scale)
{
    var w = imageObject.width;
    var h = imageObject.height;

    var ctx = context;

    // save state
    ctx.save();
    // set screen position
    ctx.translate(x, y);
    // set rotation
    ctx.rotate(Math.PI / 180.0 * rotation);
    // set scale value
    ctx.scale(zoomFactor.x, zoomFactor.y);
    // draw image to screen drawImage(imageObject, sourceX, sourceY, sourceWidth, sourceHeight,
    // destinationX, destinationY, destinationWidth, destinationHeight)
    ctx.drawImage(imageObject, 0, 0, w, h, -w/2, -h/2, w, h);
    // restore state
    ctx.restore();
}
