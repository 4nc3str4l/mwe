/**
 * Created by cristian on 03/04/2015.
 */

//Vector 2 maths
var Vector2 = function(x, y){
    this.x = x || 0;
    this.y  = y || 0;
};

//Rectangle
var Rectangle = function(x, y, w, h) {
    this.pos = new Vector2(x, y);
    this.w = w;
    this.h = h;
};

//Used to calculate positions
Rectangle.prototype.intersects = function(rect) {
    return !( rect.pos.x            >  ( this.pos.x + this.w) ||
            ( rect.pos.x + rect.w ) <    this.pos.x           ||
              rect.pos.y            >  ( this.pos.y + this.h) ||
            ( rect.pos.y + rect.h ) <    this.pos.y);
};

//transform
var Transform = function(x, y , w , h, rot){
  this.rect = new Rectangle(x, y , w, h);
  this.rotation = rot;
};

Transform.prototype.translate = function(vec2){
  this.rect.pos.x += vec2.x;
  this.rect.pos.y += vec2.y;
};

Transform.prototype.getPosition = function(){
  return this.rect.pos;
};

Transform.prototype.setPosition = function(vec2){
  this.rect.pos.x = vec2.x;
  this.rect.pos.y = vec2.y;
};
