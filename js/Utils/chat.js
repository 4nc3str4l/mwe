$(document).ready(function(){

  function sendMessage(){
    var message = $("#message").val();
    if( message != ""){
         $("#messages").append("<p>"+$("#message").val()+"</p>");
         $("#message").val("");
    }
  }

  function scrollChat(){
      var pos = $("#messages").scrollTop();
      $("#messages").scrollTop(pos+100  );
  }

  $("#send").click(function(){
    sendMessage();
  });

  $(document).keypress(function(e) {
    if(e.which == 13) {
      sendMessage();
    }
  });
  setInterval(scrollChat, 100);
});
