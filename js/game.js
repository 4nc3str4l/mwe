
window.onload = function(){

    //############################################################################
    //#                            MAIN GAME LOGIC                               #
    //############################################################################
	var Game = function(){

		var player = new Player();
        var obstacle = new Obstacle(0, 0);

		var map = new Map("images/bg.jpg");

		function isGamePaused(){
			if(keys.indexOf(80) > -1){
				paused = !paused;
				if(paused) setTimeout(update, 1000);
			}
		}

        function updateCollisions(){
        }

		function update(){
			++ups;
			isGamePaused();
			if(!paused){
				player.update();
                obstacle.update();
                updateCollisions();
			}
		}

		function randomColorScreen(){
			context.fillStyle = '#'+Math.floor(Math.random()*16777215).toString(16);
			context.fillRect(0,0,canvas.width, canvas.height);
		}

		function clearScreen(){
			context.fillStyle = "#000";
			context.fillRect(0, 0, canvas.width, canvas.height);
		}

		function render(){
			++fps;
			if(!paused){
				clearScreen();
				player.render();
                obstacle.render();
			}
		}

		this.start = function(){
			setInterval(render, 1);
			setInterval(update, 16);
			if(DEBUG){
				$("#debugBox").css("visibility", "visible");
				setInterval(function(){
					$("#fps_counter").html(ups+"up/s"+" | "+fps+"fp/s");
					fps = 0;
					ups = 0;
				},1000);
			}
		};
	}

    //############################################################################
    //#                         GAME INICIALITZATION                             #
    //############################################################################

    game = new Game();
    key = new Key();
	game.start();
};
