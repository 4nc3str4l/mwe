/**
 * The map class
 * @param background_image
 * @constructor
 */
var Map = function(background_image){

    //create a new image for the map background
    var img = new Image();
    img.src = background_image;

    //how zommed is our map ???
    var zoomX = 900;
    var zoomY = 600;

    //the original position of our map
    var screenPosx = 0;
    var screenPosy = 0;

    //the superfamous render function
    this.render = function(x, y){
        console.log("Map position: ("+x+","+y+")");
        context.drawImage(
            img,
            100, 100,
            zoomX, zoomY,
            screenPosx, screenPosy,
            canvas.width, canvas.height
        );
    }
}
